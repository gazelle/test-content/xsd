2018-06-28 (TQN ELCA)
-------------------------------
+ spid-query-2-0.wsdl (add)
+ eCH-0214-2-0.xsd (add)
	++ Introduction du type noticeType qui d�crit une remarque
	++ Possibilit� d'ajouter une ou plusieurs remarques globales  dans la r�ponse
	++ Possibilit� d'ajouter une ou plusieurs remarques globales dans la r�ponse de chacune des sous-requ�tes


2018-05-24 (DBN ELCA)
-------------------------------
eCH-0215-2-0.xsd

2018-04-30 (DBN ELCA)
-------------------------------
eCH-0212-1-0.xsd

2018-03-29 (DBN ELCA)
-------------------------------
Update eCH-0058-4-0.xsd (Ausgabedatum: 17.02.2012)

2017-08-03 (DBN ELCA)  
-------------------------------
rc-UPI-> rc-upi
rc-upi-0000-1-0.xsd
rc-upi-0001-1-0.xsd
spid-query.wsdl
wupisuppl.wsdl

2017-07-20 (TQN ELCA)
-------------------------------
Add new XSD/WSDL for rc-upi
rc-upi-0000-1-0.xsd
rc-upi-0001-1-0.xsd
wupisuppl.wsdl

WSDL for UPI/SPID
spid-management.wsdl
spid-query.wsdl

2017-01-13 (DBN ELCA)
-------------------------------
Add new XSD/WSDL for UPI/SPID
+ eCH-0007-5-0.xsd
+ eCH-0008-3-0.xsd
+ eCH-0010-5-1.xsd
+ eCH-0011-8-1.xsd
+ eCH-0021-7-0.xsd
+ eCH-0044-4-1.xsd
+ eCH-0058-5-0.xsd
+ eCH-0135-1-0.xsd
+ eCH-0213-1-0.xsd
+ eCH-0213-commons-1-0.xsd
+ eCH-0214-1-0.xsd
+ UPI-spid-declaration.wsdl
+ UPI-spid-query.wsdl

2015-09-22 (DBN ELCA)
-------------------------------   
eCH-0086-1-7.xsd: eCH-0084-commons-1-4

2015-09-10 (DBN ELCA)
-------------------------------   
eCH-0084-1-7.xsd/eCH-0084-commons-1-4.xsd: newPerson: originalName, fathersName, mothersName, dateOfDeath nillable="true"
eCH-0084-1-7.xsd: mergePersonOrUpdateLocalPersonId: max = 2

2015-09-09 (DBN ELCA)
-------------------------------   
eCH-0086-1-7.xsd: firstNames nillable="true"

2015-05-12 (DBN ELCA)
-------------------------------  
eCH-0084-commons-1-4.xsd : foreignBirthTown nillable="true"
eCH-0084-1-7.xsd: eCH-0084-commons-1-4

2015-05-12 (DBN ELCA)
-------------------------------  
eCH-0084-1-7.xsd: 
new request type:  mergePersonOrUpdateLocalPersonId
ahvvn optional for:  
�	updateCurrentValuesRequest 
�	updateLocalPersonIdRequest
�	addEntryToHistoryRequest
�	updateCurrentValuesOrAddEntryToHistoryRequest
�	erasePersonRequest
�	eraseLocalPersonId
�	mergePersonsRequest
�	mergePersonOrUpdateLocalPersonId

2015-04-24 (DBN ELCA)
-------------------------------  
eCH-0084-1-7.xsd: new fields for AddEntryToHistory/updateCurrentValuesOrAddEntryToHistory

2015-04-16 (DBN ELCA)
------------------------------- 
UPI-20402-0-1.xsd: valuesStoredUnderAhvvn
eCH-0084-1-7.xsd: reason 5 (updateCurrentValuesRequest/updateCurrentValuesOrAddEntryToHistory)

2015-03-05 (DBN ELCA)
------------------------------- 
UPI-20402-0-1.xsd: valuesStoredUnderAhvvn

2013-08-30 (DBN ELCA)
------------------------------- 
eCH-0086-1-6.xsd: originalName, mothersName, fathersName and dateOfDeath nillable="true"

2013-03-07 (DBN ELCA)
-------------------------------
eCH-0084-commons-1-3.xsd: typeOfRecord + shownDocument
eCH-0084-1-6.xsd: typeOfRecord + shownDocument
eCH-0086-1-5.xsd: typeOfRecord + shownDocument

2012-08-30 (DBN ELCA)
-------------------------------
eCH-0084-1-5.xsd: 
	- MergePersonRequest: ahvvn facultatif 

2012-06-29 (DBN ELCA)
-------------------------------
eCH-0084-1-5.xsd: 
	- Header : nouveau tag orderingTimestamp, pour toutes les annonces eCH0084, annonces et r�ponses
	- reason = 4 pour adoption (updateCurrentValuesRequest/updateCurrentValuesOrAddEntryToHistory nouvelle valeur possible et newPersonRequest nouveau tag)
	- noSearchDone pour newPersonRequest
eCH-0044-3-0.xsd

2011-06-09 (DBN ELCA)
-------------------------------
eCH-0084-1-4.xsd/eCH-0084-commons-1-2.xsd: "reason" (updateCurrentValuesRequest et updateCurrentValuesOrAddEntryToHistoryRequest) peut valoir 3, le fichier "commons" est inchang�
eCH-0085-1-4.xsd: nouvelle op�ration getSource
eCH-0086-1-3.xsd: modification dans les commentaires
 


2011-02-08 (DBN ELCA)
- nouveau tag localPersonId (eCH-86: m�me version 1-3) 
	eCH-0086-1-3.xsd


2010-10-13 (DBN ELCA)
-------------------------------
-ajout du eCH-0083-1-2.xsd (clearing)

2010-09-02 (DBN ELCA)
-------------------------------
- nationalit� facultative (eCH-0084, eCH-0085, eCH-0086)
- nouveau tag sourceIdToCompareWith (eCH-86)
	eCH-0084-1-4.xsd
	eCH-0085-1-4.xsd
	eCH-0086-1-3.xsd
	eCH-0084-commons-1-2.xsd
	

2010-02-16
-------------------------------
- '/' accept� dans �tat nominatif p�re et m�re
	  ajout eCH-0084-1-3.xsd
	  ajout eCH-0085-1-3.xsd 
	  ajout eCH-0086-1-2.xsd
2009-12-10
-------------------------------
- Update (attribut Version  + commentaires)  
	  eCH-0084-1-2.xsd
	  eCH-0085-1-2.xsd 
	  eCH-0086-1-1.xsd 
2009-10-05
-------------------------------
- "regularCluster-1-1.xsd" pointe vers "eCH-0084-1-2.xsd".

2009-09-30
-------------------------------
- Added:
	+ eCH-0084-1-1.xsd
	+ eCH-0084-1-2.xsd
	+ eCH-0085-1-1.xsd
	+ eCH-0085-1-2.xsd
- UPI-declaration.wsdl:
	+ Point to eCH-0084-1-2.xsd
- UPI-query.wsdl:
	+ Point to eCH-0085-1-1.xsd

	
2009-03-16 (Igor Metz, Glue AG)
-------------------------------

* eCH-0086/1 
  New version eCH-0086-1-1.xsd, which obsoletes eCH-0086-1-0.xsd. 
  changed: /refusedType/reason enumerated values added, documentation
  adapted accordingly.

2009-02-02 (Igor Metz, Glue AG)
-------------------------------

* eCH-0087/1 
  New version eCH-0087-1-2.xsd, which obsoletes eCH-0087-1-1.xsd. 
  changed: /response/movedIn is now of type movedInTyp (was: movedType)
  
2008-11-28 (Igor Metz, Glue AG)
-------------------------------

* eCH-0020/1, eCH-0021/2, eCH-0058/1, eCH-0078/1
  Schema files added. These files are the released version of these standards.

* eCH-0084/1, eCH-0085/1, eCH-0086/1
  baseNameUPI_Type changed:
  a name consists of at least one letter from the base alphabeth. No resitrictions are
  imposed on the usage of spaces, hyphens or full stops.

* eCH-0083/1
  - new version 1.1 of schema with filename eCH-0083-1-1.xsd introduced. This version
    of the schema will be used in April 2009 to transmit the responsed from ZAS/CdC
    to the person registers.
  - baseNameUPI_Type changed:
    a name consists of at least one letter from the base alphabeth. No resitrictions are
    imposed on the usage of spaces, hyphens or full stops.

* eCH-0087/1
  New version 1.1 with filename eCH-0087-1-1.xsd, which replaces version 1.0

2008-11-06 (Igor Metz, Glue AG)
-------------------------------

* eCH-0090/2
  Changes since v 1.0:
  - namespace URN changed.
  - envelope: pattern in participantIdType allows additional functional code for sedex
    system ids (i.e. 0-sedex-*)
  - receipt: status code is now a numeric range to allow for introduction
    of additional status codes without changing the XML schema.
  - new version number attributes for envelope and receipt

2008-10-01 (Igor Metz, Glue AG)
-------------------------------

* eCH-0083/1 (eCH-0083-1-0.xsd)
  Change:
        - compound names (de: Doppelnamen, fr: noms doubles) can be provided with a single
          hyphen (e.g. "Friedman-Montana") or with a double hyphen (e.g. "Friedman--Montana")

2008-08-26 (Igor Metz, Glue AG)
-------------------------------

* eCH-0084/1
  new optional element updateCurrentValuesOrAddEntryToHistoryRequest/localPersonId
  added.

2008-07-15 (Igor Metz, Glue AG)
-------------------------------

* eCH-0087/1
  - File renamed to eCH-0087-1-0.xsd to match eCH naming conventions
  - new elements requestRecordType/buildingName and requestRecordType/insuranceNumber added
  - element requestRecordType/dwellingID replaced by requestRecordType/dwellingNumber

2008-06-09 (Igor Metz, Glue AG)
-------------------------------

* eCH-0084/1
  - /response/refused/reason:
    Code "10" (= service (database) not available) added.
  - /updateCurrentValuesRequest/person/dateOfBirth
    changed from nillable="true" to nillable="false" to match documentation.

* eCH-0085/1
  - /getAhvnResponse/refused/reason
    Corrected enumeration to match documentation.
  - /getInfoPersonResponse/refused/reason
    Corrected enumeration to match documentation.
  - /searchPersonResponse/refused/reason
    Corrected enumeration to match documentation.

2008-05-09 (Igor Metz, Glue AG)
-------------------------------

* eCH-0086/1
  Bugreport von H. Huber (W&W Informatik AG). Korrigiert: Typ von
  compareDataRequest/dataToCompare/dateOfBirth

2008-02-21 (Igor Metz, Glue AG)
-------------------------------

* eCH-0083/1
  Neu. Schema f�r die Erstzuteilung der AHVN13.

* eCH-0084/1
  Neu. Schema f�r die UPI (unique person identifier) Declaration Schnittstelle.

* eCH-0085/1
  Neu. Schema f�r die UPI (unique person identifier) Query Schnittstelle.

* eCH-0086/1
  Neu. Schema f�r die UPI (unique person identifier) Compare Schnittstelle.


2008-01-09 (Igor Metz, Glue AG)
-------------------------------

* eCH-0071/1, eCH-0072/1
  Entwicklerkommentare entfernt, BFS Copyright eingef�gt.

2007-12-17 (Igor Metz, Glue AG)
-------------------------------

* eCH-0071/1
    - Dokumentationskommentare eingef�gt.

* eCH-0072/1
    - Dokumentationskommentare eingef�gt.
    - Basistyp von countryType/remark* auf token ge�ndert.

* eCH-0090/1
	- Neuer Statuscode 312.

2007-11-16 (Igor Metz, Glue AG)
-------------------------------

* eCH-0011/3
  - religionType ge�ndert: ist nummerisch mit min. 3 und max. 6 Ziffern.
  - dwellingAddressType ge�ndert: neue Struktur f�r Angabe von EGID, EWID
    und houldholdId.

2007-10-29 (Igor Metz, Glue AG)
-------------------------------

* eCH-0007 liegt in einer neuen Version vor: eCH-0007/3 (eCH-0007-3-0.xsd)
  (�nderungswunsch BFS)
  Modifikation an swissMunicipalityType: municipalityId und
  cantonAbbreviation sind optional. Grund: Bei Personen, die vor 1960 zugezogen
  sind, ist es m�glich, dass die Nummer der Gemeinde und der Kanton nicht
  bekannt sind.

* eCH-0008 liegt in einer neuen Version vor: eCH-0008/2 (eCH-0008-2-0.xsd)
  (�nderungswunsch BFS)
  Modifikation an countryType: countryId ist optional. Grund: f�r L�nder,
  die vor 1945 verschwunden sind, gibt es keine L�nder-Nummer, z.B.
  �sterreich-Ungarn.

* eCH-0010 liegt in einer neuen Version vor: eCH-0010/3 (eCH-0010-3-0.xsd)
  (�nderungswunsch BFS)
  neuer Typ swissAddressInformationType f�r Domiziladressen in der Schweiz

* eCH-0011 liegt in einer neuen Version vor: eCH-0011/3 (eCH-0011-3-0.xsd)
  (�nderungswunsch BFS)
  - Bez�ge auf neue Versionen der Schemas eCH-0007, eCH-0008, eCH-0010
  - religionType enth�lt keine Aufz�hlung mehr, sondern ist nur noch als
    Pattern definiert
  - //dwellingAddress/mailAddress umbenannt auf //dwellingAddress/address,
    da es sich um eine (physische) Domiziladress (dh. kein Postfach!) und
    nicht eine Postadresse (event. Postfachadresse) handelt.
  - dwellingAddressType verwendet neuen Typ eCH-0010:swissAddressInformationType
  - dwellingAddressType: Umstrukturierung mit EGID, EWID und householdId
  - birthPlaceType: bei Schweizern muss country immer CH sein; municipality
    verwendet neu den Typ eCH-0007:swissMunicipalityType

* eCH-0044 wurde erweitert, Versionsnummer bleibt gleich (eCH-0044-1-0.xsd)
  (�nderungswunsch eCH)
  personIdentificationType enth�lt neu ein optionales Element EuPersonId.
  Dies ist eine r�ckw�rtskompatible Erweiterung.

* eCH-0099 liegt in neuer Version vor: eCH-0099-1-1.xsd
  - das Element /eCH-0099:delivery verwendet neu den Typ
    eCH-0011:reportedPersonType aus eCH-0011/3. Die vorher
    vorhandene eigene Version dieses Typs ist entfernt worden.
  - das Element /eCH-0099:delivery hat neu die Version 1.1
    (@version="1.1")
  - neue Versionen der eCH Schemas werden verwendet.
  - /delivery/reportedPerson/person/contact ist optional (gem�ss
    eCH-0011). Das Element muss aber vorhanden sein, wenn keine
    Domiziladresse vorhanden ist (EGID = 999999999).

* eCH-0071: Datenstandard Nomenklatur - Historisiertes Gemeindeverzeichnis
  der Schweiz.
  Entwurf, noch nicht abgenommen.

* eCH-0072: Datenstandard Nomenklatur - Staaten und Gebiete.
  Entwurf, noch nicht abgenommen.

